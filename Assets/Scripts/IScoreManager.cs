﻿using UnityEngine;

public interface IScoreManager
{
    #region IScoreManager

    void AddScore(int value = 0);

    void RemoveScore(int value);

    void Reinit();

    int Score { get; }

    #endregion
}
