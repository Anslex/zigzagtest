﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Locator : MonoBehaviour
{
    #region Locator

    public IScoreManager ScoreManager => _scoreManager;

    [SerializeField]
    private ScoreManager _scoreManager;

    #endregion
}
