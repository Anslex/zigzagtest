﻿using UnityEngine;

public class Chunk : MonoBehaviour
{
    #region Chunk

    public Transform[] Begin;
    public Transform[] End;
    public AnimationCurve ChanceFromDistance;
    public Transform[] _gemsPlacements;

    [SerializeField]
    private Gem _gem;

    private int _index = 0;

    #endregion

    #region IChunk

    public void CreateGem(int index)
    {
        var newGem = Instantiate(_gem);

        newGem.transform.parent = Config.GEMSSPAENRANDOM ? _gemsPlacements[Random.Range(0, _gemsPlacements.Length - 1)].transform
                : _gemsPlacements[index].transform;
        newGem.transform.localPosition = Vector3.zero;

        _index++;
        
        if(_index > 4)
        {
            _index = 0;
        }
    }

    public void OnCreate()
    {
        //MayBeAnimationShow
    }

    public void BeforeDestroy()
    {
        //MayBeAnimationHide
        Destroy(gameObject);
    }

    #endregion
}
