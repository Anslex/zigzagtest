﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gem : MonoBehaviour
{
    #region Gem

    private IScoreManager _scoreManager;

    private Locator _locator;

    private void Start()
    {
        _locator = FindObjectOfType<Locator>(); // I know need soo good injection...
        _scoreManager = _locator.ScoreManager;
    }

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            _scoreManager.AddScore();
            Destroy(gameObject);
        }
    }

    #endregion
}
