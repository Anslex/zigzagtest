﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    #region PlayerController

    [SerializeField]
    private Rigidbody _componentRigidbody;

    [SerializeField]
    private int _velocity = 4;

    [SerializeField]
    private MapGenerator _mapGenerator;

    private Vector3 _playerVelocity;

    private IScoreManager _scoreManager;

    private Locator _locator;

    private void Start()
    {
        _playerVelocity = _componentRigidbody.velocity;
        _playerVelocity.z = _velocity;
        _playerVelocity.y = -1;

        _locator = FindObjectOfType<Locator>(); // I know need soo good injection...
        _scoreManager = _locator.ScoreManager;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _playerVelocity.z = _playerVelocity.z == 0 ? _velocity : 0;
            _playerVelocity.x = _playerVelocity.x == 0 ? _velocity : 0;
        }

        if (transform.position.y < 0.4f)
        {
            _componentRigidbody.velocity = Vector3.zero;
        }
        else
        {
            _componentRigidbody.velocity = _playerVelocity;
        }
    }

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "Respawn")
        {
            transform.position = Vector3.up;
            _mapGenerator.Respawn();
            _scoreManager.Reinit();
        }
    }

    #endregion
}
