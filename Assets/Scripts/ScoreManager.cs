﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour, IScoreManager
{
    #region ScoreManager

    [SerializeField]
    private Text _textScore;

    private void Start()
    {
        UpdateScoreText();
    }

    private void UpdateScoreText()
    {
        _textScore.text = Score.ToString();
    }

    #endregion

    #region IScoreManager

    //if need in future
    public int Score { get; private set; } = 0;

    public void AddScore(int value = 0)
    {
        Score += value == 0 ? Config.SCOREPERGEM : value;
        UpdateScoreText();
    }

    public void Reinit()
    {
        Score = 0;
        UpdateScoreText();
    }

    public void RemoveScore(int value)
    {
        Score -= value;
        UpdateScoreText();
    }

    #endregion
}
