﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleCamera : MonoBehaviour
{
    #region SimpleCamera

    [SerializeField]
    private Transform _target;

    private void Update()
    {
        transform.position = _target.position;
    }

    #endregion
}
