﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MapGenerator : MonoBehaviour
{
    #region MapGenerator

    [SerializeField]
    private Transform _player;
    [SerializeField]
    private Chunk[] _chunkPrefabs;
    [SerializeField]
    private Chunk _firstChunk;

    [SerializeField]
    private Chunk _firstChunkForRespawn;

    [SerializeField]
    private Gem _gem;

    private List<Chunk> _spawnedChunks = new List<Chunk>();

    private const int DeltaPosPlayerForSpawn = 20;
    private const int CountChunksForDel = 35;

    private int _index = 0;

    public void Respawn()
    {
        foreach(var chunk in _spawnedChunks)
        {
            chunk.BeforeDestroy();
        }

        _spawnedChunks.Clear();

        var newChunk = Instantiate(_firstChunkForRespawn);
        newChunk.transform.position = Vector3.zero;
        _spawnedChunks.Add(newChunk);
    }

    private void Start()
    {
        _spawnedChunks.Add(_firstChunk);
    }

    private void Update()
    {
        if (_player.position.z > _spawnedChunks[_spawnedChunks.Count - 1].End[0].position.z - DeltaPosPlayerForSpawn)
        {
            SpawnChunk();
        }
    }

    private void SpawnChunk()
    {
        var newChunk = Instantiate(GetRandomChunk());
        newChunk.OnCreate();  
        var direction = Random.Range(0, _spawnedChunks[_spawnedChunks.Count - 1].End.Length);
        newChunk.transform.position = _spawnedChunks[_spawnedChunks.Count - 1]
            .End[direction]
            .position - newChunk.Begin[direction].localPosition;
        _spawnedChunks.Add(newChunk);
        newChunk.CreateGem(_index);

        _index++;

        if (_index > 4)
        {
            _index = 0;
        }

        if (_spawnedChunks.Count >= CountChunksForDel)
        {
            _spawnedChunks[0].BeforeDestroy();
            _spawnedChunks.RemoveAt(0);
        }
    }

    private Chunk GetRandomChunk()
    {
        List<float> chances = new List<float>();

        for (int i = 0; i < _chunkPrefabs.Length; i++)
        {
            chances.Add(_chunkPrefabs[i].ChanceFromDistance.Evaluate(_player.transform.position.z));
        }

        float value = Random.Range(0, chances.Sum());
        float sum = 0;

        for (int i = 0; i < chances.Count; i++)
        {
            sum += chances[i];
            if (value < sum)
            {
                return _chunkPrefabs[i];
            }
        }

        return _chunkPrefabs[_chunkPrefabs.Length - 1];
    }

    #endregion
}
